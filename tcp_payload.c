#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include<netinet/tcp.h>   //Provides declarations for tcp header
#include<netinet/ip.h>    //Provides declarations for ip header
#include <sys/socket.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

#define DEBUG_ETH_HEADER 0
#define DEBUG_IP_HEADER 0
#define DEBUG_TCP_HEADER 0

static int s = -1;

void onexit(int signum)
{
	(void)signum;
	printf("Exiting");
	close(s);
}

int main()
{
	char buf[65536];
	ssize_t recv_size = -1;

	int ret = 0, i;

	struct sockaddr_ll socket_address;

	s = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_IP));

	if(s == -1)
	{
		perror("Socket creation failed");
		exit (0);
	}

	signal(SIGINT, onexit);

	memset(&socket_address, 0, sizeof (socket_address));
	socket_address.sll_family = PF_PACKET;
	socket_address.sll_ifindex = if_nametoindex("lo");
	socket_address.sll_protocol = htons(ETH_P_IP);

	ret = bind(s, (struct sockaddr*)&socket_address,
			sizeof(socket_address));
	if (ret == -1)
	{
		perror("Bind");
		exit (0);
	}

	while (1)
	{
		memset(&buf, 0, sizeof(buf));
		recv_size = recv(s, &buf, sizeof(buf), 0);
		if (recv_size == -1)
		{
			perror("Socket receive");
			exit (0);
		}

#if 0
		//packet data
		printf("\n");
		for(i=0; i<recv_size; i++)
		{
			printf("%02x ", buf[i]);
		}
#endif

		//Ethernet header
		struct ethhdr *eth = (struct ethhdr *)(buf);
#if DEBUG_ETH_HEADER
		printf("\nEthernet Header\n");
		printf("\t|-Source Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->h_source[0],eth->h_source[1],eth->h_source[2],eth->h_source[3],eth->h_source[4],eth->h_source[5]);
		printf("\t|-Destination Address : %.2X-%.2X-%.2X-%.2X-%.2X-%.2X\n",eth->h_dest[0],eth->h_dest[1],eth->h_dest[2],eth->h_dest[3],eth->h_dest[4],eth->h_dest[5]);
		printf("\t|-Protocol : %d\n",eth->h_proto);
#endif

		//IP header
		unsigned short iphdrlen;
		struct sockaddr_in source, dest;
		struct iphdr *ip = (struct iphdr*)(buf + sizeof(struct ethhdr));
		memset(&source, 0, sizeof(source));
		source.sin_addr.s_addr = ip->saddr;
		memset(&dest, 0, sizeof(dest));
		dest.sin_addr.s_addr = ip->daddr;

#if DEBUG_IP_HEADER
		printf("\nIP Header\n");
		printf("\t|-Version : %d\n",(unsigned int)ip->version);
		printf("\t|-Internet Header Length : %d DWORDS or %d Bytes\n",(unsigned int)ip->ihl,((unsigned int)(ip->ihl))*4);
		printf("\t|-Type Of Service : %d\n",(unsigned int)ip->tos);
		printf("\t|-Total Length : %d Bytes\n",ntohs(ip->tot_len));
		printf("\t|-Identification : %d\n",ntohs(ip->id));
		printf("\t|-Time To Live : %d\n",(unsigned int)ip->ttl);
		printf("\t|-Protocol : %d\n",(unsigned int)ip->protocol);
		printf("\t|-Header Checksum : %d\n",ntohs(ip->check));
		printf("\t|-Source IP : %s\n", inet_ntoa(source.sin_addr));
		printf("\t|-Destination IP : %s\n",inet_ntoa(dest.sin_addr));
#endif
		//TCP
		if(ip->protocol == IPPROTO_TCP)
		{
			//TCP header
			/* getting actual size of IP header*/
			iphdrlen = ip->ihl*4;
			/* getting pointer to tcp header*/
			struct tcphdr *tcp=(struct tcphdr*)(buf + iphdrlen + sizeof(struct ethhdr));

#if DEBUG_TCP_HEADER
			printf("\nTCP Header\n");
			printf("\t|-Source Port : %d\n" , ntohs(tcp->source));
			printf("\t|-Destination Port : %d\n" , ntohs(tcp->dest));
			printf("\t|-TCP Checksum : %d\n" , ntohs(tcp->check));
#endif

			//Payload
			unsigned char * data = (buf + iphdrlen + sizeof(struct ethhdr) + sizeof(struct tcphdr));
			int remaining_data = recv_size - (iphdrlen + sizeof(struct ethhdr) + sizeof(struct tcphdr));

			for(i=0;i<remaining_data;i++)
			{
				if(i!=0 && i%8==0)
					printf("\n");
				printf("0x%.2X ",data[i]);
			}
			printf("\n=======================================\n");
		}
		//UDP
		else if(ip->protocol == IPPROTO_UDP)
			printf("This is a UDP packet!\n");
		else
			printf("Unknow packet!\n");
	}

	return 0;
}
