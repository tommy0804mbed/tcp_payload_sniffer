#!/bin/bash
##################################
# Test case 1
# sending tcp packet to localhost
###################################
data="hello! tommy"

#TCP payload capture
sudo ./tcp_payload 1> payload.txt&
process=$(pidof tcp_payload)
sleep 1

#Server side
nc -l -p6667&
sleep 1

#Client side
echo -ne $data | netcat 127.0.0.1 6667
sleep 1

sudo kill -2 $process

#Todo, Compare data with payload


